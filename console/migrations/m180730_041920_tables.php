<?php
use yii\db\Migration;

/**
 * Class m180730_041920_tables
 */
class m180730_041920_tables extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 ENGINE=InnoDB';
        }

	$this->createTable( 'recipe' , [
		'id' => $this->bigPrimaryKey( )->unsigned( ) ,
		'title' => $this->string( )->notNull( ) ,
		'duration' => $this->integer( )->unsigned( )->notNull( ) ,
		'food_list' => $this->text( )->notNull( )
	] , $tableOptions ) ;

	$this->createTable( 'food' , [
		'id' => $this->bigPrimaryKey( )->unsigned( ) ,
		'title' => $this->string( )->notNull( )
	] , $tableOptions ) ;

	$this->createTable( 'recipe_food' , [
		'recipe_id' => $this->bigInteger( )->unsigned( )->notNull( ) ,
		'food_id' => $this->bigInteger( )->unsigned( )->notNull( )
	] , $tableOptions ) ;

	$this->createIndex( 'idx_food_title' , 'food' , [ 'title' ] , true ) ;
	$this->execute( '
ALTER TABLE
	`recipe`
ADD
	FULLTEXT INDEX `ft_food_list`( `food_list` ) ;
	' ) ;

	$this->addPrimaryKey(
		'pk-recipe_food' ,
		'recipe_food' ,
		[ 'recipe_id' , 'food_id' ]
	) ;

        $this->addForeignKey(
		'fk-recipe_food_id' ,
		'recipe_food' ,
		'food_id' ,
		'food' ,
		'id' ,
		'CASCADE'
        ) ;

        $this->addForeignKey(
		'fk-recipe_id_food' ,
		'recipe_food' ,
		'recipe_id' ,
		'recipe' ,
		'id' ,
		'CASCADE'
        ) ;
    }

    public function down()
    {
	$this->dropTable( 'recipe_food' ) ;
	$this->dropTable( 'food' ) ;
	$this->dropTable( 'recipe' ) ;
    }
}
