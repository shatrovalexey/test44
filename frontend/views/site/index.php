<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index table-responsive">
    <div class="body-content">
	<form action="#" method="post" id="search-recipe">
		<fieldset>
			<legend>
				<h3>поиск рецептов</h3>
				<p>выделите продукты для фильтра по рецептам
			</legend>

			<label class="block">
				<span>продукты</span>
				<div>
					<select data-src="/?r=site/foodlist" name="foods[]" required id="foods" style="min-width: 200px ; max-width: 400px ;" multiple size="10"></select>
					<div class="both"></div>
				</div>
			</label>
		</fieldset>
	</form>

	<table class="table nod" id="recipe-table" data-src="/?r=site/recipelist">
		<caption>
			<h3>Рецепты</h3>
			<p>список обновляется каждые 10 секунд
		</caption>
		<thead>
			<tr>
				<th>название</th>
				<th>время приготовления</th>
				<th>продукты</th>
			</tr>
		</thead>
		<tbody>
			<tr class="nod">
				<td data-key="title"></td>
				<td data-key="duration"></td>
				<td data-key="foods"></td>
			</tr>
		</tbody>
	</table>
    </div>
</div>
