jQuery( function( ) {
	let $recipe_table = jQuery( "#recipe-table" ) ;
	let $recipe_table_tbody = $recipe_table.find( "tbody" ) ;
	let $recipe_table_tr = $recipe_table_tbody.find( "tr.nod" ) ;
	let $recipe_table_reload = function( $callback ) {
		$recipe_table.find( "tbody tr" ).not( ":first" ).remove( ) ;

		jQuery.ajax( {
			"url" : $recipe_table.data( "src" ) ,
			"data" : {
				"foods" : jQuery( "#foods" ).val( )
			} ,
			"success" : function( $data ) {
				jQuery( $data ).each( function( $i , $data_item ) {
					let $tr_node = $recipe_table_tr.clone( true ).removeClass( "nod" ) ;

					$tr_node.find( "*[data-key]" ).each( function( $i , $td_item ) {
						jQuery( $td_item ).text( $data_item[ jQuery( $td_item ).data( "key" ) ] ) ;
					} ) ;

					$recipe_table.append( $tr_node ) ;
				} ) ;

				$recipe_table.removeClass( "nod" ) ;
			}
		} ) ;

		try {
			$callback( ) ;
		} catch( $exception ) {
		}
	} ;

	let $food_reload = function( ) {
		let $self = jQuery( "#foods" ) ;

		jQuery.ajax( {
			"url" : $self.data( "src" ) ,
			"success" : function( $data ) {
				$self.find( "option" ).remove( ) ;

				jQuery( $data ).each( function( $i , $item ) {
					$self.append(
						jQuery( "<option>" ).text( $item.title ).val( $item.id )
					) ;
				} ) ;
			}
		} ) ;
	} ;

	$recipe_table.each( function( ) {
		$recipe_table_reload( function( ) {
			setInterval( $recipe_table_reload , 10e3 ) ;
		} ) ;

		return false ;
	} ) ;

	$food_reload( ) ;

	jQuery( "#add-food" ).on( "click" , function( ) {
		let $self = jQuery( this ) ;
		let $title = prompt( $self.attr( "title" ) ) ;

		if ( ! $title ) {
			return false ;
		}

		jQuery.ajax( {
			"url" : $self.data( "action" ) ,
			"type" : "POST" ,
			"data" : {
				"title" : $title
			} ,
			"success" : $food_reload
		} ) ;

		return false ;
	} ) ;

	jQuery( "#add-recipe" ).submit( function( ) {
		let $self = jQuery( this ) ;

		jQuery.ajax( {
			"url" : $self.attr( "action" ) ,
			"type" : $self.attr( "method" ) ,
			"data" : $self.serialize( ) ,
			"success" : $recipe_table_reload
		} ) ;

		return false ;
	} ) ;
} ) ;