<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%recipe}}".
 *
 * @property string $id
 * @property string $title
 * @property string $duration
 *
 * @property RecipeFood[] $recipeFoods
 * @property Food[] $foods
 */
class Recipe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%recipe}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'duration'], 'required'],
            [['duration'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'duration' => 'Duration',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipeFoods()
    {
        return $this->hasMany(RecipeFood::className(), ['recipe_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoods()
    {
        return $this->hasMany(Food::className(), ['id' => 'food_id'])->viaTable('{{%recipe_food}}', ['recipe_id' => 'id']);
    }


	public static function getList( $food_ids = array( ) ) {
		$recipes = [ ] ;

		$query = self::find( ) ;

		if ( ! empty( $food_ids ) ) {
			$query->where( '
MATCH( `food_list` ) AGAINST( :food_ids IN BOOLEAN MODE )
			' )->addParams( [ ':food_ids' => '+"' . implode( '" +"' , array_map( function( $food_id ) {
				return sha1( $food_id ) ;
			} , $food_ids ) ) . '"' ] ) ;
		}

		foreach ( $query->all( ) as $recipe_item ) {
			$recipes[] = array(
				'title' => $recipe_item->title ,
				'id' => $recipe_item->id ,
				'duration' => $recipe_item->duration ,
				'foods' => implode( ', ' , array_map( function( $item ) {
					return $item->title ;
				} , $recipe_item->getFoods( )->all( ) ) )
			) ;
		}

		return $recipes ;
	}

    /**
     * {@inheritdoc}
     * @return RecipeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RecipeQuery(get_called_class());
    }
}
