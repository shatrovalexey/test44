<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%recipe_food}}".
 *
 * @property string $recipe_id
 * @property string $food_id
 *
 * @property Food $food
 * @property Recipe $recipe
 */
class RecipeFood extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%recipe_food}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['recipe_id', 'food_id'], 'required'],
            [['recipe_id', 'food_id'], 'integer'],
            [['recipe_id', 'food_id'], 'unique', 'targetAttribute' => ['recipe_id', 'food_id']],
            [['food_id'], 'exist', 'skipOnError' => true, 'targetClass' => Food::className(), 'targetAttribute' => ['food_id' => 'id']],
            [['recipe_id'], 'exist', 'skipOnError' => true, 'targetClass' => Recipe::className(), 'targetAttribute' => ['recipe_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'recipe_id' => 'Recipe ID',
            'food_id' => 'Food ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFood()
    {
        return $this->hasOne(Food::className(), ['id' => 'food_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipe()
    {
        return $this->hasOne(Recipe::className(), ['id' => 'recipe_id']);
    }

    /**
     * {@inheritdoc}
     * @return RecipeFoodQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RecipeFoodQuery(get_called_class());
    }
}
