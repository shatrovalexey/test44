<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%food}}".
 *
 * @property string $id
 * @property string $title
 *
 * @property RecipeFood[] $recipeFoods
 * @property Recipe[] $recipes
 */
class Food extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%food}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipeFoods()
    {
        return $this->hasMany(RecipeFood::className(), ['food_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipes()
    {
        return $this->hasMany(Recipe::className(), ['id' => 'recipe_id'])->viaTable('{{%recipe_food}}', ['food_id' => 'id']);
    }

	public static function getList( ) {
		return self::find( )->orderBy( 'title' )->all( ) ;
	}

    /**
     * {@inheritdoc}
     * @return FoodQuery the active query used by this AR class.
     */
	/*
    public static function find()
    {
        return new FoodQuery(get_called_class());
    }
	*/
}
