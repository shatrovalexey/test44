<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\Recipe ;
use common\models\Food ;
use common\models\RecipeFood ;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
		    [
			'allow' => true ,
			'actions' => [
				'recipelist' ,
				'foodlist' ,
				'recipecreate' ,
				'foodcreate'
			] ,
			'roles' => ['@']
		    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	protected function __json( $data ) {
		$response = Yii::$app->response ;
		$response->format = \yii\web\Response::FORMAT_JSON ;
		$response->data = $data ;

		return $response ;
	}

	public function actionFoodlist( ) {
		return $this->__json( Food::getList( ) ) ;
	}

	public function actionRecipelist( ) {
		return $this->__json( Recipe::getList( ) ) ;
	}

	public function actionFoodcreate( ) {
		$food = new Food( ) ;
		$food->title = Yii::$app->request->post( 'title' ) ;

		return $this->__json( $food->upsert( ) ) ;
	}

	public function actionRecipecreate( ) {
		$recipe = new Recipe( ) ;
		$recipe->title = Yii::$app->request->post( 'title' ) ;
		$recipe->duration = Yii::$app->request->post( 'duration' ) ;
		$recipe->food_list = implode( ',' , array_map( function( $food_id ) {
			return sha1( $food_id ) ;
		} , Yii::$app->request->post( 'foods' ) ) ) ;
		$recipe->insert( ) ;

		foreach ( Yii::$app->request->post( 'foods' ) as $food_id ) {
			$recipe_food = new RecipeFood( ) ;
			$recipe_food->recipe_id = $recipe->id ;
			$recipe_food->food_id = $food_id ;
			$recipe_food->insert( ) ;
		}

		return $this->__json( $recipe->id ) ;
	}

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index' ) ;
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
