<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index table-responsive">
	<form method="post" action="/?r=site/recipecreate" id="add-recipe">
		<fieldset>
			<legend>
				<h3>новый рецепт</h3>
			</legend>

			<label>
				<input name="title" required placeholder="название">
			</label>
			<label>
				<input name="duration" required type="number" max="200" min="1" placeholder="время приготовления">
			</label>
			<label class="block">
				<span>продукты</span>
				<div>
					<a href="#add-food" id="add-food" title="укажите название продукта" data-action="/?r=site/foodcreate" style="float: right ;">новый продукт</a>
					<select data-src="/?r=site/foodlist" name="foods[]" required id="foods" style="min-width: 200px ; max-width: 400px ;" multiple size="10"></select>
					<div class="both"></div>
				</div>
			</label>

			<label>
				<span>создать</span>
				<input type="submit" value="&rarr;">
			</label>
		</fieldset>
	</form>

	<table class="table nod" id="recipe-table" data-src="/index.php?r=site/recipelist">
		<caption>
			<h3>Рецепты</h3>
		</caption>
		<thead>
			<tr>
				<th>название</th>
				<th>время приготовления</th>
				<th>продукты</th>
			</tr>
		</thead>
		<tbody>
			<tr class="nod">
				<td data-key="title"></td>
				<td data-key="duration"></td>
				<td data-key="foods"></td>
			</tr>
		</tbody>
	</table>
</div>